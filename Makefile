TARGET    = parse
BUILD_DIR = build

.PHONY: all clean

all: parse

clean:
	$(RM) -r $(TARGET) $(BUILD_DIR)

$(TARGET):
	ghc -o $@ -outputdir $(BUILD_DIR) main.hs parser.hs
