{--------------------------------------------------------------------------
Parser for BitTorrent files.
This file is part of TorrentParser.
Copyright © 2015 Mariano Street <mctpyt@gmail.com>.
License GNU GPLv3+.

Reference documentation
=======================

- http://www.bittorrent.org/beps/bep_0003.html
- http://fileformats.wikia.com/wiki/Torrent_file
---------------------------------------------------------------------------}


module Parser (V(..), Parser.length, Parser.lookup, parse, parseFromFile)
where


import qualified Data.Functor.Identity  as D  (Identity)

import qualified Text.Parsec            as P  (parse)
import qualified Text.Parsec.Char       as PC (anyChar, char, digit,
                                               endOfLine)
import qualified Text.Parsec.Combinator as PM (count, eof, option, optional,
                                               manyTill)
import qualified Text.Parsec.Error      as PE (ParseError)
import qualified Text.Parsec.String     as PS (parseFromFile)
import qualified Text.Parsec.Prim       as PP (ParsecT)
import           Text.Parsec.Prim             ((<|>))


----------------------------------------------------------------------------
-- Types                                                                  --
----------------------------------------------------------------------------

data V = StringV     { string     :: String   }
       | IntegerV    { integer    :: Int      }
       | ListV       { list       :: [V]      }
       | DictionaryV { dictionary :: [(V, V)] }

type Parser a = PP.ParsecT String () D.Identity a


----------------------------------------------------------------------------
-- Helper functions for `V`                                               --
----------------------------------------------------------------------------

length :: V -> Int
length = Prelude.length . list

lookup :: String -> V -> Maybe V
lookup k = l . dictionary
    where l []                     = Nothing
          l ((StringV k', v) : ys) | k == k'   = Just v
                                   | otherwise = l ys


----------------------------------------------------------------------------
-- Parser components                                                      --
----------------------------------------------------------------------------

parseString :: Parser V
parseString = do lengthS <- PM.manyTill PC.digit $ PC.char ':'
                 let length = read lengthS
                 s <- PM.count length PC.anyChar
                 return $ StringV s

parseInteger :: Parser V
parseInteger = do PC.char 'i'
                  sign <- PM.option id $ PC.char '-' >> return negate
                  iS   <- PM.manyTill PC.digit $ PC.char 'e'
                  let i = read iS
                  return $ IntegerV $ sign i

parseList :: Parser V
parseList = do PC.char 'l'
               xs <- PM.manyTill parseValue $ PC.char 'e'
               return $ ListV xs

parseDictionary :: Parser V
parseDictionary = do PC.char 'd'
                     ps <- PM.manyTill parsePair $ PC.char 'e'
                     return $ DictionaryV ps

parsePair :: Parser (V, V)
parsePair = do x <- parseString
               y <- parseValue
               return (x, y)

parseValue :: Parser V
parseValue = parseString <|> parseInteger <|> parseList <|> parseDictionary

parseTorrent :: Parser V
parseTorrent = do d <- parseDictionary
                  PM.optional PC.endOfLine
                  PM.eof
                  return d

parse :: String -> Either PE.ParseError V
parse = P.parse parseTorrent "<input>"

parseFromFile = PS.parseFromFile parseTorrent
