{--------------------------------------------------------------------------
Parser for BitTorrent files.
This file is part of TorrentParser.
Copyright © 2015 Mariano Street <mctpyt@gmail.com>.
License GNU GPLv3+.

Usage
=====

Command line invocation::

    $ ./parse [ACTION [binary]]

``ACTION`` can be:

    print   Print torrent metadata prettily.
    export  Export to JSON.
    list    List files in torrent.

If ommitted, ``ACTION`` is assumed to be ``print``.

``binary`` is an option for reading input in binary mode instead of text
mode.

The program reads the contents of a BitTorrent file from standard input.
---------------------------------------------------------------------------}


module Main where


import qualified Parser             as P

import qualified Data.List          as L (intercalate)

import qualified System.Environment as E (getArgs)
import qualified System.IO          as I (hSetBinaryMode, stdin)


----------------------------------------------------------------------------
-- User actions                                                           --
----------------------------------------------------------------------------

indentation :: String
indentation = "    "

pathSeparator :: String
pathSeparator = "/"

-- Print torrent metadata prettily.
doPrint :: P.V -> IO ()
doPrint x = pp x 0 >> putStrLn ""
    where indent :: Int -> IO ()
          indent n = do putStrLn ""
                        putStr $ concat $ take n $ repeat indentation
          pp :: P.V -> Int -> IO ()
          pp (P.StringV s)     _ = putStr $ "\"" ++ s ++ "\""
          pp (P.IntegerV i)    _ = putStr $ show i
          pp (P.ListV [])      _ = putStr "L: []"
          pp (P.ListV [x])     n = do putStr "L: "
                                      pp x n
          pp (P.ListV xs)      n = do putStr "L:"
                                      ppl xs (n + 1)
          pp (P.DictionaryV d) n = do putStr "D:"
                                      ppd d (n + 1)
          ppl []       _ = return ()
          ppl (y : ys) n = do indent n
                              pp y n
                              ppl ys n
          ppd []            _ = return ()
          ppd ((k, v) : ys) n = do indent n
                                   pp k n
                                   putStr " -> "
                                   pp v n
                                   ppd ys n

-- Export to JSON.
doExport :: P.V -> IO ()
doExport = putStrLn . e
    where e (P.StringV s)     = "\"" ++ s ++ "\""
          e (P.IntegerV i)    = show i
          e (P.ListV xs)      = "[" ++ L.intercalate "," (map e xs) ++ "]"
          e (P.DictionaryV d) = "{" ++ L.intercalate "," (map ep d) ++ "}"
          ep (k, v) = e k ++ ":" ++ e v

-- List files in torrent.
doList :: P.V -> IO ()
doList d = let Just info = P.lookup "info" d
               Just name = P.lookup "name" info
           in case P.lookup "files" info of
                  Just files -> do putStr $ show $ P.length files
                                   putStrLn " files:"
                                   listFiles name files
                  Nothing -> do putStrLn "1 file:"
                                doPrint name
    where listFiles prefixV = mapM_ (f (P.string prefixV)) . P.list
          f prefix d = let Just path = P.lookup "path" d
                       in putStrLn $ g prefix path
          g prefix = L.intercalate pathSeparator
                     . (prefix:) . map P.string . P.list


----------------------------------------------------------------------------
-- Program execution                                                      --
----------------------------------------------------------------------------

optionBinary :: [String] -> Bool
optionBinary []         = False
optionBinary ["binary"] = True

main :: IO ()
main = do -- Parse command line arguments for deciding what to do.
          args <- E.getArgs
          let (action, o) = case args of
                                []            -> (doPrint,  [])
                                "print"  : xs -> (doPrint,  xs)
                                "export" : xs -> (doExport, xs)
                                "list"   : xs -> (doList,   xs)
          -- Read and parse input.
          if optionBinary o then I.hSetBinaryMode I.stdin True
                            else return ()
          l <- getContents
          case P.parse l of
              Right v -> action v
              Left  e -> putStrLn $ "ERROR: " ++ show e
